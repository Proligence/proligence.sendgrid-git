﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using Orchard;
using Orchard.Localization;
using Orchard.Logging;
using Orchard.Security;
using Orchard.Themes;
using Proligence.SendGrid.Services;

namespace Proligence.SendGrid.Controllers
{
    [Themed(false)]
    [AlwaysAccessible]
    public class EventController : Controller
    {
        private readonly ISendGridEventsHandler _handler;
        private readonly IOrchardServices _services;

        public EventController(
            IOrchardServices services, 
            ISendGridEventsHandler handler)
        {
            _handler = handler;
            _services = services;
            T = NullLocalizer.Instance;
            Logger = NullLogger.Instance;
        }

        public Localizer T { get; set; }
        public ILogger Logger { get; set; }

        [HttpPost]
        public HttpStatusCodeResult Handle()
        {
            Request.InputStream.Position = 0;
            var content = new StreamReader(Request.InputStream).ReadToEnd();
            var contexts = JsonConvert.DeserializeObject<List<EventContext>>(content);
            foreach (var context in contexts)
            {
                try
                {
                    _handler.Call(context);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex,
                        T("Error when handling SendGrid event '{0}' for e-mail {1}", context.Event, context.Email).Text);
                    _services.TransactionManager.Cancel();
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
