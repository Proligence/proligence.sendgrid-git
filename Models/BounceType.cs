﻿namespace Proligence.SendGrid.Services
{
    public enum BounceType
    {
        Bounce,
        Blocked,
        Expired
    }
}