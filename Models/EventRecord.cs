﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Orchard.Data.Conventions;

namespace Proligence.SendGrid.Models
{
    public class EventRecord
    {
        public EventRecord()
        {
            EventData = new List<EventDataRecord>();
        }

        public virtual int Id { get; set; }
        public virtual string UniqueId { get; set; }
        public virtual EventType Event { get; set; }
        public virtual string Email { get; set; }
        public virtual string Category { get; set; }

        public virtual DateTime CreatedDate { get; set; }

        [JsonIgnore]
        [CascadeAllDeleteOrphan]
        public virtual IList<EventDataRecord> EventData { get; set; }
    }
}