﻿namespace Proligence.SendGrid.Models
{
    public enum EventType
    {
        None,
        Processed,
        Deferred,
        Delivered,
        Open,
        Click,
        Bounce,
        Dropped,
        SpamReport,
        Unsubscribe
    }
}