﻿namespace Proligence.SendGrid.Models
{
    public class EventModel
    {
        public EventType Event { get; set; }
        public string Email { get; set; }
    }
}