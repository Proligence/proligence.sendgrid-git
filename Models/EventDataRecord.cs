﻿using Newtonsoft.Json;
using Orchard.Data.Conventions;

namespace Proligence.SendGrid.Models
{
    public class EventDataRecord
    {
        [JsonIgnore]
        public virtual int Id { get; set; }

        [JsonIgnore]
        public virtual EventRecord EventRecord { get; set; }
        public virtual string DataKey { get; set; }

        [StringLengthMax]
        public virtual string Value { get; set; }
    }
}