﻿namespace Proligence.SendGrid.Models
{
    using System;
    using System.Collections.Generic;

    public class SendGridHistoryEntry
    {
        public string Email { get; set; }
        public string Event { get; set; }
        public string UniqueId { get; set; }
        public string Category { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid UserId { get; set; }
        public Guid MailingId { get; set; }
        public IDictionary<string, string> Data { get; set; }
    }
}