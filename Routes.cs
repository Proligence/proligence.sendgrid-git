﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Orchard.Mvc.Routes;

namespace Proligence.SendGrid
{
    public class Routes : IRouteProvider
    {
        public void GetRoutes(ICollection<RouteDescriptor> routes)
        {
            foreach (RouteDescriptor routeDescriptor in GetRoutes())
            {
                routes.Add(routeDescriptor);
            }
        }

        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            return new[]
                   {
                       new RouteDescriptor
                       {
                           Priority = 50,
                           SessionState = SessionStateBehavior.Disabled,
                           Route = new Route(
                               "Api/SendGrid/Event",
                               new RouteValueDictionary
                               {
                                   {"area", "Proligence.SendGrid"},
                                   {"controller", "Event"},
                                   {"action", "Handle"},
                               },
                               new RouteValueDictionary(),
                               new RouteValueDictionary
                               {
                                   {"area", "Proligence.SendGrid"}
                               },
                               new MvcRouteHandler())
                       }
                   };
        }
    }
}