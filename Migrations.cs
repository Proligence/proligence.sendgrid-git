using System;
using JetBrains.Annotations;
using Orchard.Data.Migration;

namespace Proligence.SendGrid {
    [UsedImplicitly]
    public class Migrations : DataMigrationImpl {
        public int Create()
        {
            SchemaBuilder.CreateTable("EventRecord", table => table
                .Column<int>("Id", c => c.PrimaryKey().Identity())
                .Column<string>("UniqueId")
                .Column<string>("Event")
                .Column<string>("Email")
                .Column<string>("Category")
                .Column<DateTime>("CreatedDate"));

            return 1;
        }

        public int UpdateFrom1()
        {
            SchemaBuilder.CreateTable("EventDataRecord", table => table
                .Column<int>("Id", c => c.PrimaryKey().Identity())
                .Column<string>("DataKey")
                .Column<string>("Value", c => c.Unlimited())
                .Column<int>("EventRecord_Id"));

            SchemaBuilder.AlterTable("EventDataRecord", t => t.CreateIndex("IX_EventDataRecord_EventKey", "EventRecord_id", "DataKey"));
            SchemaBuilder.AlterTable("EventDataRecord", t => t.CreateIndex("IX_EventDataRecord_Key", "DataKey"));

            return 2;
        }
    }
}