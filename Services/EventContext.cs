﻿using System;
using System.Collections.Generic;
using Proligence.SendGrid.Models;

namespace Proligence.SendGrid.Services
{
    public class EventContext : Dictionary<string, object>
    {
        public EventContext() : base(StringComparer.OrdinalIgnoreCase)
        {}

        public EventType Event { 
            get
            {
                EventType enumValue;
                return ContainsKey("event") && Enum.TryParse(this["event"].ToString(), true, out enumValue) 
                    ? enumValue 
                    : default(EventType);
            }
            set {
                this["event"] = value.ToString();
            } 
        }
        public string Email
        {
            get
            {
                return ContainsKey("email") ? this["email"].ToString() : null;
            }
            set
            {
                this["email"] = value;
            }
        }
        public string UniqueId
        {
            get
            {
                return ContainsKey("uniqueid") ? this["uniqueid"].ToString() : null;
            }
            set
            {
                this["uniqueid"] = value;
            }
        }
        public string Category
        {
            get
            {
                return ContainsKey("category") ? this["category"].ToString() : null;
            }
            set
            {
                this["category"] = value;
            }
        }
    }
}