﻿using System;
using System.Linq;
using JetBrains.Annotations;
using Orchard.Data;
using Orchard.Services;
using Proligence.SendGrid.Models;

namespace Proligence.SendGrid.Services
{
    [UsedImplicitly]
    public class DefaultSendGridEventsHandler : ISendGridEventsHandler
    {
        private readonly IRepository<EventRecord> _repository;
        private readonly IClock _clock;

        public DefaultSendGridEventsHandler(IRepository<EventRecord> repository, IClock clock)
        {
            _repository = repository;
            _clock = clock;
        }

        public void Processed(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void Dropped(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void Delivered(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void Deferred(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void Bounce(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void Open(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void Click(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void SpamReport(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        public void Unsubscribe(EventContext ctx)
        {
            StoreRecord(ctx);
        }

        private void StoreRecord(EventContext context)
        {
            var record = new EventRecord
            {
                Category = context.Category,
                Email = context.Email,
                CreatedDate = _clock.UtcNow,
                Event = context.Event,
                UniqueId = context.UniqueId
            };

            var keys = context.Keys.Where(k =>
                !k.Equals("event", StringComparison.OrdinalIgnoreCase) &&
                !k.Equals("category", StringComparison.OrdinalIgnoreCase) &&
                !k.Equals("email", StringComparison.OrdinalIgnoreCase) &&
                !k.Equals("uniqueid", StringComparison.OrdinalIgnoreCase));

            foreach (var key in keys)
            {
                record.EventData.Add(new EventDataRecord
                                     {
                                         EventRecord = record,
                                         DataKey = key,
                                         Value = context[key].ToString()
                                     });
            }

            _repository.Create(record);
        }
    }
}