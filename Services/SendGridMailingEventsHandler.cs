﻿using System;
using System.Collections.Generic;
using System.Linq;
using Orchard.Data;
using Proligence.SendGrid.Models;

namespace Proligence.SendGrid.Services
{
    public class SendGridMailingEventsHandler : IMailingEventsHandler
    {
        private readonly IRepository<EventDataRecord> _eventDataRepo;

        public SendGridMailingEventsHandler(IRepository<EventDataRecord> eventDataRepo)
        {
            _eventDataRepo = eventDataRepo;
        }

        public IEnumerable<dynamic> Send(dynamic context)
        {
            yield break;
        }

        public IEnumerable<dynamic> History(dynamic context)
        {
            return FetchEvents(context.Filter);
        }

        private IEnumerable<dynamic> FetchEvents(IDictionary<string, IEnumerable<string>> filter)
        {
            // Need to know which properties to query on EventRecord
            var eventRecordFilterKeys = typeof(EventRecord).GetProperties()
                .Where(p => filter.ContainsKey(p.Name))
                .Select(p => p.Name);

            var eventDataRecordFilterKeys =
                filter.Keys.Where(k => !eventRecordFilterKeys.Contains(k, StringComparer.OrdinalIgnoreCase));

            var eventsQuery = eventDataRecordFilterKeys
                .Aggregate(
                    _eventDataRepo.Table,
                    (current, key) => current.Where(r => r.DataKey.Equals(key) && r.Value.Equals(filter[key])));

            foreach (var key in eventRecordFilterKeys)
            {
                if (key.Equals("event", StringComparison.OrdinalIgnoreCase))
                {
                    var enums = filter[key]
                        .Select(entry =>
                                {
                                    EventType eventType;
                                    return Enum.TryParse(entry, true, out eventType) ? (EventType?) eventType : null;
                                })
                        .Where(s => s.HasValue)
                        .Select(s => s.Value)
                        .ToList();

                    eventsQuery = eventsQuery.Where(r => enums.Count > 0 && enums.Contains(r.EventRecord.Event));
                }

                if (key.Equals("category", StringComparison.OrdinalIgnoreCase))
                {
                    eventsQuery = eventsQuery.Where(r => filter[key].Contains(r.EventRecord.Category));
                }

                if (key.Equals("email", StringComparison.OrdinalIgnoreCase))
                {
                    eventsQuery = eventsQuery.Where(r => filter[key].Contains(r.EventRecord.Email));
                }

                if (key.Equals("uniqueid", StringComparison.OrdinalIgnoreCase))
                {
                    eventsQuery = eventsQuery.Where(r => filter[key].Contains(r.EventRecord.UniqueId));
                }
            }

            return eventsQuery
                .ToList()
                .GroupBy(r => r.EventRecord)
                .Select(g => new SendGridHistoryEntry
                {
                    Email = g.Key.Email,
                    Event = g.Key.Event.ToString(),
                    UniqueId = g.Key.UniqueId,
                    Category = g.Key.Category,
                    CreatedDate = g.Key.CreatedDate,
                    Data = g.ToDictionary(edr => edr.DataKey, edr => edr.Value)
                });

        }
    }
}