﻿using System.Collections.Generic;
using Orchard.Events;

namespace Proligence.SendGrid.Services
{
    /// <summary>
    /// Describes mailing-related events.
    /// </summary>
    public interface IMailingEventsHandler : IEventHandler
    {
        /// <summary>
        /// Called when a given mailing is being sent to a particular e-mail address.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        IEnumerable<dynamic> Send(dynamic context);

        /// <summary>
        /// Retrieves history entries for a given mailing.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        IEnumerable<dynamic> History(dynamic context);
    }
}