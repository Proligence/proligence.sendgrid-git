﻿using System;
using Orchard.Events;
using Proligence.SendGrid.Models;

namespace Proligence.SendGrid.Services
{
    public interface ISendGridEventsHandler : IEventHandler
    {
        void Processed(EventContext context);
        void Dropped(EventContext context);
        void Delivered(EventContext context);
        void Deferred(EventContext context);
        void Bounce(EventContext context);
        void Open(EventContext context);
        void Click(EventContext context);
        void SpamReport(EventContext context);
        void Unsubscribe(EventContext context);
    }

    public static class SendGridEventsExtensions
    {
        public static void Call(this ISendGridEventsHandler handler, EventContext context)
        {
            switch (context.Event)
            {
                case EventType.Bounce:
                    handler.Bounce(context);
                    break;
                case EventType.Click:
                    handler.Click(context);
                    break;
                case EventType.Deferred:
                    handler.Deferred(context);
                    break;
                case EventType.Delivered:
                    handler.Delivered(context);
                    break;
                case EventType.Dropped:
                    handler.Dropped(context);
                    break;
                case EventType.Open:
                    handler.Open(context);
                    break;
                case EventType.Processed:
                    handler.Processed(context);
                    break;
                case EventType.SpamReport:
                    handler.SpamReport(context);
                    break;
                case EventType.Unsubscribe:
                    handler.Unsubscribe(context);
                    break;
                default:
                    throw new InvalidOperationException(string.Format("Invalid event type: {0}", context.Event));
            }
        }
    }
}